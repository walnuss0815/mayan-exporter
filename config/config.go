package config

type Config struct {
	Mayan struct {
		Url            string `yaml:"url" env:"MAYAN_EXPORTER_URL" env-default:"http://localhost"`
		Username       string `yaml:"username" env:"MAYAN_EXPORTER_USERNAME" env-default:"admin"`
		Password       string `yaml:"password" env:"MAYAN_EXPORTER_PASSWORD" env-default:"passw0rd"`
		IgnoreTlsError bool   `yaml:"ignore_tly_error" env:"MAYAN_EXPORTER_IGNORE_TLS_ERROR" env-default:"false"`
	} `yaml:"mayan"`
	Exporter struct {
		Directory string `yaml:"directory" env:"MAYAN_EXPORTER_DIRECTORY" env-default:"."`
	} `yaml:"exporter"`
}

package exporter

import (
	api "gitlab.com/walnuss0815/mayan-edms-client/client"
	"gitlab.com/walnuss0815/mayan-edms-client/models"
)

type Exporter struct {
	api api.Client
}

func NewExporter(url string, username string, password string, ignoreTlsError bool) *Exporter {
	i := new(Exporter)
	i.api = *api.NewClient(url, username, password, ignoreTlsError)
	return i
}

func (c Exporter) GetDocuments() ([]models.Document, error) {
	return c.api.GetDocuments()
}

func (c Exporter) DownloadDocument(document models.Document, filePath string) error {
	return c.api.DownloadVersion(document.LatestVersion, filePath)
}

module gitlab.com/walnuss0815/mayan-exporter

go 1.15

require (
	github.com/ilyakaznacheev/cleanenv v1.2.5
	gitlab.com/walnuss0815/mayan-edms-client v0.0.0-20201215213640-b9ad8ab238f1
	golang.org/x/net v0.0.0-20201209123823-ac852fbbde11 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
	olympos.io/encoding/edn v0.0.0-20201019073823-d3554ca0b0a3 // indirect
)

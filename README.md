# Mayan Exporter

The Mayan Exporter helps to export documents from a remote [Mayan EDMS](https://www.mayan-edms.com/) server.

## Parameter

- `-verbose` - Enable logging of already existing documents
- `-redownload` - Force redownload of all documents

## Configuration

The Mayan Exporter can be configured by a YAML file or environment variables. Ab example YAML configuration file can be found in the [repository](/-/blob/master/config.yaml.template).
package main

import (
	"flag"
	"fmt"
	"github.com/ilyakaznacheev/cleanenv"
	"gitlab.com/walnuss0815/mayan-exporter/config"
	"gitlab.com/walnuss0815/mayan-exporter/exporter"
	"log"
	"os"
)

var redownload bool
var verbose bool

func init() {
	flag.BoolVar(&redownload, "redownload", false, "force redownload documents")
	flag.BoolVar(&verbose, "verbose", false, " increase verbosity")
}

func main() {
	flag.Parse()

	var cfg config.Config
	err := cleanenv.ReadEnv(&cfg)
	if err != nil {
		log.Panicf("Error loading config: %s", err.Error())
	}

	configFile := "config.yaml"
	_ = cleanenv.ReadConfig(configFile, &cfg)

	exp := exporter.NewExporter(cfg.Mayan.Url, cfg.Mayan.Username, cfg.Mayan.Password, cfg.Mayan.IgnoreTlsError)

	log.Print("Fetching documents...")
	documents, err := exp.GetDocuments()
	if err != nil {
		log.Printf("Error fetching documents: %s", err.Error())
	}

	log.Printf("Found %v documents", len(documents))

	documentDownloadCount := 0

	for _, document := range documents {
		directory := fmt.Sprintf("%s/%v/%s", cfg.Exporter.Directory, document.Id, document.LatestVersion.Checksum)

		err := os.MkdirAll(directory, os.ModePerm)
		if err != nil {
			log.Printf("Error creating directory: %s", err.Error())
		}

		filePath := directory + "/" + document.Label

		if _, err := os.Stat(filePath); os.IsNotExist(err) || redownload {
			err = exp.DownloadDocument(document, filePath)
			if err != nil {
				log.Printf("Error downloading document %s (%v): %s", document.Label, document.Id, err.Error())
			}

			documentDownloadCount += 1

			log.Printf("Downloaded %s (%v) to %s", document.Label, document.Id, filePath)
		} else {
			if verbose {
				log.Printf("Document %s already exists in %s", document.Label, filePath)
			}
		}
	}

	log.Printf("Downloaded %v documents", documentDownloadCount)
}
